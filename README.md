# Project 4: Brevet time calculator with Ajax

Author: Wenyu Zhang
Email: wenyuz@uoregon.edu

## ACP controle times

Calculates open time in three different situations:
*	1) control_distance = 0, open time = brevet start time
* 2) control_distance greater than brevet distance, open time is equal the time in brevet distance.
* 3) control_distance less than brevet distance, accumulate open time by each range.

Calculates close time in two different situations:
* 1) When control distance equals to brevet distance, specific 200, 400, and 1000, close time is in specific number which is different from calculation.
* 2) When control distance does not equal to brevet distance, accumulate time by each range.


## Testing

Test acp_times function by five ranges, (0,200), (200,400),(400,600),(600,100),(1000+).
And two different situations, in the range of brevet distance or over the range of brevet distance.
