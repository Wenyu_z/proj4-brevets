'''
# test file
'''
import acp_times
# import nose
import logging
import arrow
logging.basicConfig(format='%(levelname)s:%(message)s',level=logging.WARNING)
log = logging.getLogger(__name__)

date = arrow.get("2020-01-01 00:00", "YYYY-MM-DD HH:mm")
# date = "2020-01-01 00:00"

def test_open():
    assert acp_times.open_time(20,200,date) == date.clone().shift(hours=0, minutes=35).isoformat()
    assert acp_times.open_time(111,200,date) == date.clone().shift(hours=3, minutes=16).isoformat()
    assert acp_times.open_time(205,200,date) == "2020-01-01T05:53:00+00:00"
    assert acp_times.open_time(20,400,date) == date.clone().shift(hours=0, minutes=35).isoformat()
    assert acp_times.open_time(205,400,date) == date.clone().shift(hours=6, minutes=2).isoformat()
    assert acp_times.open_time(410,400,date) == date.clone().shift(hours=12, minutes=8).isoformat()
    # print(acp_times.open_time(551,600,date))
    assert acp_times.open_time(551,600,date) == date.clone().shift(hours=17, minutes=10).isoformat()
    assert acp_times.open_time(630,600,date) == "2020-01-01T18:48:00+00:00"
    assert acp_times.open_time(20,1000,date) == "2020-01-01T00:35:00+00:00"
    assert acp_times.open_time(630,1000,date) == "2020-01-01T19:52:00+00:00"
    assert acp_times.open_time(1010,1000,date) == "2020-01-02T09:05:00+00:00"


def test_less_200():
    '''
    test by brevet distance is 200
    '''
    assert acp_times.open_time(60,200,date) == date.clone().shift(hours=1, minutes=46).isoformat()
    assert acp_times.open_time(111,200,date) == date.clone().shift(hours=3, minutes=16).isoformat()

    # print(acp_times.close_time(60,200,date))
    # print(type(date.clone().shift(hours=0, minutes=35)))
    assert acp_times.close_time(60,200,date) == date.clone().shift(hours=4, minutes=0).isoformat()
    assert acp_times.close_time(111,200,date) == date.clone().shift(hours=7, minutes=24).isoformat()



def test_over_200():
    # print(acp_times.close_time(205,200,date))
    assert acp_times.open_time(205,200,date) == "2020-01-01T05:53:00+00:00"
    assert acp_times.close_time(205,200,date) == date.clone().shift(hours=13, minutes=30).isoformat()

def test_less_400():

    assert acp_times.open_time(60,400,date) == date.clone().shift(hours=1, minutes=46).isoformat()
    assert acp_times.open_time(205,400,date) == date.clone().shift(hours=6, minutes=2).isoformat()

    assert acp_times.close_time(60,400,date) == date.clone().shift(hours=4, minutes=00).isoformat()

    assert acp_times.close_time(205,400,date) == date.clone().shift(hours=13, minutes=40).isoformat()

def test_over_400():
    # print(acp_times.open_time(410,400,date))

    assert acp_times.open_time(410,400,date) == date.clone().shift(hours=12, minutes=8).isoformat()
    assert acp_times.close_time(410,400,date) == date.clone().shift(hours=27, minutes=0).isoformat()

def test_less_600():

    assert acp_times.open_time(551,600,date) == date.clone().shift(hours=17, minutes=10).isoformat()
    assert acp_times.close_time(551,600,date) == "2020-01-02T12:44:00+00:00"

def test_over_600():
    # print(acp_times.open_time(630,600,date))
    assert acp_times.open_time(630,600,date) == "2020-01-01T18:48:00+00:00"
    assert acp_times.close_time(630,600,date) == "2020-01-02T16:00:00+00:00"


def test_less_1000():

    assert acp_times.open_time(60,1000,date) == "2020-01-01T01:46:00+00:00"
    assert acp_times.close_time(60,1000,date) == "2020-01-01T04:00:00+00:00"
    # print(acp_times.close_time(890,1000,date))
    assert acp_times.open_time(630,1000,date) == "2020-01-01T19:52:00+00:00"
    assert acp_times.close_time(630,1000,date) == "2020-01-02T18:38:00+00:00"

def test_over_1000():

    assert acp_times.open_time(1010,1000,date) == "2020-01-02T09:05:00+00:00"
    assert acp_times.close_time(1010,1000,date) == "2020-01-04T03:00:00+00:00"

if __name__ == '__main__':
    test_less_200()
    test_over_200()
    test_less_400()
    test_over_400()
    test_less_600()
    test_over_600()
    test_less_1000()
    test_over_1000()
